#!/usr/bin/env python
# -*- coding: utf-8 -*-

# In[2]:


import wmfdata

spark = wmfdata.spark.get_session(
    app_name='research-quids',
    type='yarn-large',
    extra_settings={
        'spark.sql.shuffle.partitions': 2048,
        'spark.reducer.maxReqsInFlight': 1,
        'spark.shuffle.io.retryWait': '60s'
    }
)


# In[3]:


import re
import string

import mwparserfromhell as mwp
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

# In[4]:


def get_articles(spark: SparkSession, snapshot: str, ipl_snapshot) -> DataFrame:
    'Returns all articles for lang in the given snapshot with their wikidata ids.'

    wikitext_df = spark.sql(f"""SELECT page_id, page_title, wiki_db, revision_text
    FROM wmf.mediawiki_wikitext_current
    WHERE snapshot = '{snapshot}' AND page_namespace = 0
    """)
    ipl_df = spark.sql(f"""SELECT item_id, page_id, wiki_db
    FROM wmf.wikidata_item_page_link
    WHERE snapshot = '{ipl_snapshot}' AND page_namespace = 0
    """)
    articles_df = (
        wikitext_df
        .join(ipl_df, ['page_id', 'wiki_db'], how='inner')
        .select(
            'item_id',
            'wiki_db',
            'page_title',
            'revision_text',
        )
    )
    return articles_df


# In[5]:


# ASCII punctuation to be trimmed from section headings
punctuation = string.punctuation.replace('()', ' ')
whitespace_re = re.compile(r'\s')

sections_schema = T.ArrayType(
    T.StructType([
        T.StructField('heading', T.StringType()),
        T.StructField('links', T.ArrayType(T.StringType())),
        T.StructField('pos_start', T.IntegerType()),
        T.StructField('pos_end', T.IntegerType()),
    ])
)


@F.udf(returnType=sections_schema)
def parse_wikitext(wikitext: str, sub_re=whitespace_re, strip_chars=punctuation):
    sections = mwp.parse(wikitext).get_sections(
        levels=[2], include_headings=True
    )
    total_sections = len(sections)
    parsed_sections = []
    for i, section in enumerate(sections):
        try:
            section_links = [link.title.strip_code() for link in section.filter_wikilinks()]
            section = section.strip_code()
            section_heading, *_ = section.split('\n', maxsplit=1)
            section_heading = sub_re.sub(' ', section_heading).strip(punctuation)
            parsed_sections.append(
                dict(
                    heading=section_heading,
                    links=section_links,
                    pos_start=i+1,
                    pos_end=total_sections-i)
            )
        except KeyError:  # when mwp is unable to convert an invalid html entity to unicode point
            continue
    return parsed_sections


def extract_section_attributes(articles_df: DataFrame) -> DataFrame:
    """Extracts section headings and section links for each article
    in articles_df aligns the links with their wikidata ids.
    Returns a dataframe containing array of structs {section_heading,
    section_links} for each article.
    """

    # apply udf to get section heading and links from revision text
    sections_df = articles_df.select(
        'item_id',
        'wiki_db',
        F.explode(parse_wikitext('revision_text')).alias('section')
    )
    sections_df = sections_df.select(
        'item_id',
        'wiki_db',
        F.col('section.*')
    )
    sections_df = sections_df.withColumn('heading', F.lower(F.col('heading')))
    sections_df = sections_df.cache()

    sections_df.count()

    counts_df = (
        sections_df
        .groupBy(['heading', 'wiki_db'])
        .agg(
            F.count('*').alias('count'),
            F.mean('pos_start').alias('pos_start_mean'),
            F.mean('pos_end').alias('pos_end_mean')
        )
    )
    headings_match = (F.col('sections.heading') == F.col('counts.heading')) & (F.col('sections.wiki_db') == F.col('counts.wiki_db'))
    section_attrs_df = (
        sections_df
        .alias('sections')
        .join(counts_df.alias('counts'), headings_match)
        .select(
            'sections.*',
            'counts.count',
            'counts.pos_start_mean',
            'counts.pos_end_mean'
        )
    )
    section_attrs_df = section_attrs_df.withColumn('link', F.explode_outer('links'))
    page_titles_df = articles_df.select(
        'page_title',
        'item_id',
        'wiki_db'
    )
    # align each link with its wikidata id
    section_attrs_df = (
        section_attrs_df
        .alias('sections')
        .join(
            page_titles_df.alias('page'),
            ((F.col('sections.link') == F.col('page.page_title')) &
            (F.col('sections.wiki_db') == F.col('page.wiki_db'))), how='left')
        .select(
            'sections.item_id',
            'sections.wiki_db',
            'heading',
            'pos_start_mean',
            'pos_end_mean',
            'count',
            F.col('page.item_id').alias('link'),
        )
    )
    section_attrs_df = (
        section_attrs_df
        .groupBy(['item_id', 'heading', 'wiki_db', 'count', 'pos_start_mean', 'pos_end_mean'])
        .agg(F.collect_set('link').alias('links'))
    )
    # pack each articles section attrs first into a struct and then all into an array
    section_attrs_df = section_attrs_df.groupBy(['item_id', 'wiki_db']).agg(
        F.collect_list(
            F.struct(
                F.col('heading'),
                F.col('links'),
                F.col('count'),
                F.col('pos_start_mean'),
                F.col('pos_end_mean')
            )
        ).alias('section_attributes')
    )
    return section_attrs_df


# In[6]:


# Original code
# cx_langs = spark.read.json(
#     "/user/mnz/secmap_training_data/cx_data/cx_languages.json"
# ).collect()
# langs = [l["wp-code"] for l in cx_langs]
# wiki_db = ", ".join([f"'{lang.replace('-', '_')}wiki'" for lang in langs])

# snapshot = "2022-02"
# ipl_snapshot = f"{snapshot}-28"

snapshot = '2022-05'
ipl_snapshot = f'{snapshot}-23'

sections_dir = '/user/mfossati/research'
sections_path = f'{sections_dir}/sections_{snapshot}.parquet'

articles_df = get_articles(spark, snapshot, ipl_snapshot)
section_attrs_df = extract_section_attributes(articles_df)
section_attrs_df.write.mode('overwrite').parquet(sections_path)
