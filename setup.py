# -*- coding: utf-8 -*-
from setuptools import find_packages, setup

setup(
    name='section-topics',
    packages=find_packages(),
    version='0.1.0',
    description='pyspark jobs for the section topics data pipeline',
    author='mfossati',
)
